On a ajouté une modification dans la branche master et dans la branche feature2 pour être sur qu'on a des commits.
Ensuite on a fait la commande "git rebase master" dans la branche feature2.
Et finalement on a avancé la branche master jusqu’à la branche feature2 pour avoir toutes les modifications.
fast-forward permet à la branche master sans faire le commit d’être déplacé vers le dernier commit de la branche feature2. feature2 ayant tous les commits de master plus ses propres modifications.
Pour merge on a une fusion de deux branches avec un commit qui va rassembler les deux derniers commits des deux branches.
Pour rebase on déplace les commits d'une branche sur les commits de l'autre. Et la merge d’après sert juste à rattraper les commits.
Pour supprimer la branche feature2 il faut se placer dans la branche master et utiliser "git branch -d feature2" .