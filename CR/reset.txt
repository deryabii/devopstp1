On a créé un répertoire avec un fichier fiche1 ou on a mis 123123123 puis on a commit et on a supprimé le 123123123 et on a ajoute 456456456 puis on exécute les commandes
git reset monfichier - on a ajoute le fichier avec git add et puis cette commande nous a retiré le fichier du prochain commit
git reset commitID - la même chose que avec le fichier mais ca le fait avec tous les fichiers
git reset --soft commitID - ca deplace HEAD sur le commit qu'on veut en conservant les modifications qu'on a fait
git reset --hard commitID - avec cette commande on a perdu nos modifications (456456456) et on a restore (123123123)

